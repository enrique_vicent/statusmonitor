#ifndef _MODEL_HPP_
#define _MODEL_HPP_
#include "data.hpp"
#include <ArduinoJson.h>

RTC_DATA_ATTR int sharedChannel = 8;
EspNow2MqttClient cnx = EspNow2MqttClient("eInkMon", sharedKey, gatewayMac);
StaticJsonDocument<200> doc;

void inline fillTemp(int tempIndex, const char* name, const unsigned char * image, response_OpResponse &op){
    strncpy(data.temperatures[tempIndex].key, name, KEY_LENGTH); 
    data.temperatures[tempIndex].image=image;
    data.temperatures[tempIndex].isTempFilled = op.result_code == response_Result_OK;
    if (data.temperatures[tempIndex].isTempFilled){
      data.temperatures[tempIndex].temp = atoi(op.payload);
    }
}

void inline fillBattery(int battIndex, const char* name, response_OpResponse &op){
  strncpy(data.batteries[battIndex].key, name, KEY_LENGTH); 
    data.batteries[battIndex].isFilled = op.result_code == response_Result_OK;
    if (data.batteries[battIndex].isFilled){
      data.batteries[battIndex].value = atoi(op.payload);
    }
}

void inline fillCovid(response_OpResponse &op){
  data.covid.isFilled = op.result_code == response_Result_OK;
    if (data.covid.isFilled){
      // Deserialize the JSON document
      DeserializationError error = deserializeJson(doc, op.payload);
      if(error){
        data.covid.active= -2;
      }else{
        data.covid.active=doc["active"];
        data.covid.newKills=doc["kill"];
        data.covid.newCases=doc["new"];
      }
    }
}

void mapResponse2Data( response & rsp){
  //message 1
  if (rsp.message_type == 1 && rsp.opResponses_count == 5){
    fillBattery(DATA_INDEX_battAI, "Climatizador",rsp.opResponses[0]);
    fillBattery(DATA_INDEX_battCur1, "Cort. dorm.",rsp.opResponses[1]);
    fillTemp(DATA_INDEX_tempDorm, "Dormitorio", IMG_CAMA, rsp.opResponses[2]);
    fillTemp(DATA_INDEX_tempOut, "Exterior", IMG_OUTSIDE, rsp.opResponses[3]);
    fillBattery(DATA_INDEX_battCur2, "Cort. Salon.",rsp.opResponses[4]);
  }
  // message 2 
  else if (rsp.message_type == 2 && rsp.opResponses_count == 4){
    fillCovid(rsp.opResponses[0]);
    fillTemp(DATA_INDEX_presRe, "Presion", IMG_CASA, rsp.opResponses[1]);
    fillTemp(DATA_INDEX_tempSalon, "tempSalon", IMG_CASA, rsp.opResponses[2]); 
    fillTemp(DATA_INDEX_iaq, "iaq", IMG_CASA, rsp.opResponses[3]); 
  } else {
    Serial.printf("error in mapResponse2Data, msg_type: %d, ",rsp.message_type);
  }
}

int commPendingMessages = -1;
void commDoRequest(){
    commPendingMessages = 2;
    request rq = cnx.createRequest();
    //batch 1
    rq.message_type = 1;
    rq.operations[0] = cnx.createRequestOperationSubscribeQueue("battAI");
    rq.operations[1] = cnx.createRequestOperationSubscribeQueue("battCur1");
    rq.operations[2] = cnx.createRequestOperationSubscribeQueue("tempDorm");
    rq.operations[3] = cnx.createRequestOperationSubscribeQueue("tempOut");
    rq.operations[4] = cnx.createRequestOperationSubscribeQueue("battCur2");
    rq.operations_count = 5;
    cnx.doRequests(rq);
    //batch 2
    rq.message_type = 2;
    rq.operations[0] = cnx.createRequestOperationSubscribeQueue("cov");
    rq.operations[1] = cnx.createRequestOperationSubscribeQueue("presSalon");
    rq.operations[2] = cnx.createRequestOperationSubscribeQueue("tempSalon");
    rq.operations[3] = cnx.createRequestOperationSubscribeQueue("iaqSalon");
    rq.operations_count = 4;
    cnx.doRequests(rq);
}


#endif