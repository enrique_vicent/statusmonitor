// lilyGo T5 v2.3 eink display
#ifndef _BOARD_H_
#define _BOARD_H_

//-- display spi
#define SPI_MOSI 23
#define SPI_MISO -1
#define SPI_CLK 18

//-- display address
#define ELINK_SS 5
#define ELINK_BUSY 4
#define ELINK_RESET 16
#define ELINK_DC 17

//-- sdcard
#define SDCARD_SS 13
#define SDCARD_CLK 14
#define SDCARD_MOSI 15
#define SDCARD_MISO 2

//-- button
#define BUTTON_PIN 39

#endif