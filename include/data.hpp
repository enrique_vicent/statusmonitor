#ifndef _DATA_HPP_
#define _DATA_HPP_

#include "images.h"

#define KEY_LENGTH 16
#define BATTERIES 4
#define TEMPERATURES 5
#define DATA_INDEX_battAI    0
#define DATA_INDEX_battCur1  1
#define DATA_INDEX_battCur2  2
#define DATA_INDEX_battSelf  3
#define DATA_INDEX_tempDorm  0
#define DATA_INDEX_tempOut   1
#define DATA_INDEX_tempSalon 2
#define DATA_INDEX_iaq       3 
#define DATA_INDEX_presRe    4 
#define DATA_INDEX_covActive 0

struct Battery_t {
  char key [KEY_LENGTH] = "";
  int value = 1;
  bool isFilled = false;
};
struct Temp_t {
  char key[KEY_LENGTH] = "";
  float temp = 1.0;
  float hum = 2.0;
  bool isTempFilled = false;
  bool isHumFilled = false;
  const unsigned char * image = IMG_BUG;
};
struct Covid_t {
  int active = -1;
  int newCases = -1;
  int newKills = -1;
  bool isFilled = false;
};
struct Data {
  Battery_t batteries [BATTERIES];
  Temp_t temperatures [TEMPERATURES];
  Covid_t covid;
} data;

#endif