#ifndef _VIEW_HPP_
#define _VIEW_HPP_

//#include <GxGDE0213B1/GxGDE0213B1.h>      // 2.13" b/w
//#include <GxGDEH0213B72/GxGDEH0213B72.h>  // 2.13" b/w new panel
#include <GxGDEH0213B73/GxGDEH0213B73.h>  // 2.13" b/w newer panel

// FreeFonts from Adafruit_GFX
#include <Fonts/FreeMonoBold9pt7b.h>
#include <Fonts/FreeMonoBold12pt7b.h>
#include <Fonts/FreeMonoBold18pt7b.h>
#include <Fonts/FreeMonoBold24pt7b.h>
#include <Fonts/FreeSerif9pt7b.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/FreeSans12pt7b.h>
#include <Fonts/FreeSansBold9pt7b.h>
#include <Fonts/FreeSansBold12pt7b.h>

#include "SPI.h"
#include <GxEPD.h>
#include "SD.h"
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>

#include "data.hpp"
#include "board.h"
#include "images.h"


void renderCanvas ();
void renderTemps (int top);
void renderTemp (int x ,int y, Temp_t & temp);
void renderBatts (int top, int individualHeight);
void renderBatt (int y, Battery_t & batt);
void renderData ();
void renderError (char * error); 
void renderCovid (Covid_t & cov);

GxIO_Class io(SPI, /*CS=5*/ ELINK_SS, /*DC=*/ ELINK_DC, /*RST=*/ ELINK_RESET);
GxEPD_Class display(io, /*RST=*/ ELINK_RESET, /*BUSY=*/ ELINK_BUSY);


void renderData () {
  const int individualBattpHeight = 25;
  int battBlockHeight = BATTERIES * individualBattpHeight;
  int battBlockTop = display.height() - battBlockHeight;
  int tempBlockTop = battBlockTop - (IMG_SIZE_HEIGHT + 3) *2;
  /* display size 250x122 */
  display.init(); // enable diagnostic output on Serial
  renderCanvas();
  /* top area */
  renderCovid(data.covid);
  /* temp area */
  renderTemps(tempBlockTop);
  /* batt area */
  renderBatts(battBlockTop, individualBattpHeight);
  display.update();
}

void renderCanvas () {
  display.setRotation(0);
  display.fillScreen(GxEPD_WHITE);
  display.setTextColor(GxEPD_BLACK);
}
void renderAtom(const char * label, Temp_t &data, int x, int y){
  display.setFont(&FreeSerif9pt7b);
  display.setCursor(x , y);
  if(data.isTempFilled) {
    display.printf("%s %.0f" , label, data.temp );
  }
  else {
    display.printf("%s !?" , label  );
    display.drawLine(x, y-4, display.getCursorX(), y-4, GxEPD_BLACK);
  }
}

void renderTemps (int top ) {
  renderTemp(0, top+0, data.temperatures[DATA_INDEX_tempOut]);
  renderTemp(0, top + IMG_SIZE_HEIGHT + 3, data.temperatures[DATA_INDEX_tempDorm]);
  renderTemp(display.width()/2, top + IMG_SIZE_HEIGHT + 3, data.temperatures[DATA_INDEX_tempSalon]);
  renderAtom("Iaq",data.temperatures[DATA_INDEX_iaq],display.width()/2, top+9);
  renderAtom("Pr" ,data.temperatures[DATA_INDEX_presRe],display.width()/2, top+9+10+9);
}
void renderBatts (int top, int individualBattpHeight) {
  display.setFont(&FreeSerif9pt7b);
  for (int battIndex = 0 ; battIndex < BATTERIES ;battIndex ++){
    renderBatt(top + individualBattpHeight * battIndex, data.batteries[battIndex]);
  }

}
void renderTemp (int x, int y, Temp_t & temp){
  display.drawBitmap(temp.image, x, y, IMG_SIZE_WIDTH, IMG_SIZE_HEIGHT, GxEPD_WHITE);
  display.setCursor(x + 32, y+26);
  if(temp.isTempFilled){
    display.setFont(&FreeSansBold12pt7b);
    display.printf("%1.0f",temp.temp);
  } else {
    display.setFont(&FreeSans12pt7b);
    display.printf("!?");
    display.drawLine(x,y, x+32, y+32, GxEPD_BLACK);
    display.drawLine(x,y+32, x+32, y, GxEPD_BLACK);
  }
}

void renderBatt (int y , Battery_t & batt){
  static int const fontMaxHeight = 12;
  static int const barMarginX = 1;
  static int const barTop = 17;
  static int const barHeight = 7;
  static int const underStrikeTop = fontMaxHeight/2 + 2;
  int barWidth = display.width() - 2 * barMarginX;
  
  display.setCursor(0, y+fontMaxHeight);
  display.print(batt.key);

  if(batt.isFilled){
    int fillWidth = map(batt.value, 0,100,0,barWidth);
    fillWidth = min(barWidth,fillWidth);
    fillWidth = max(0,fillWidth);

    display.drawRect(barMarginX, y+barTop, barWidth, barHeight, GxEPD_BLACK);
    display.fillRect(barMarginX, y+barTop, fillWidth, barHeight, GxEPD_BLACK);
    for (int i = 3; i ; i--){
      int whiteBarTop = y+barTop + 1;
      int whiteBarHeight = barHeight - 3;
      int whiteBarBottom = whiteBarTop + whiteBarHeight;
      int whiteBarX = barWidth * i / 4  + barMarginX;
      display.drawLine(whiteBarX,whiteBarTop, whiteBarX, whiteBarBottom, GxEPD_WHITE);
    }
  } else {
    display.drawLine(0, y + underStrikeTop, display.getCursorX(), y + underStrikeTop, GxEPD_BLACK);
  }
}

void renderCovid (Covid_t & cov){
  const int sectionTop = 10;
  if (cov.isFilled){
    display.setCursor(38, sectionTop);
    display.print("Nuevos Covid");
    display.setCursor(6,sectionTop+26+12);
    display.printf("%i enfermos" , cov.active );
    display.setCursor(6,sectionTop+26+12 +12);
    display.printf("%i muertos hoy" , cov.newKills );
    display.setFont(&FreeSansBold12pt7b);
    display.drawBitmap(IMG_BUG, 6, sectionTop, IMG_SIZE_WIDTH, IMG_SIZE_HEIGHT, GxEPD_WHITE);
    display.setCursor(6 + 32, sectionTop+26);
    display.printf("%i",cov.newCases);


  } else {
    display.drawBitmap(IMG_BUG, 6, sectionTop, IMG_SIZE_WIDTH, IMG_SIZE_HEIGHT, GxEPD_WHITE);
    display.setCursor(6 + 32, sectionTop+26);
    display.printf(" (No data) ");
  }
}

void renderError (char * error){
  /* display size 250x122 */
  display.init(); // enable diagnostic output on Serial
  renderCanvas();
  display.setCursor(10,10);
  display.setFont(&FreeSerif9pt7b);
  display.print(error);
  display.update();
}

#endif
