// include library, include base class, make path known


#include "EspNow2MqttClient.hpp"

#include "secrets.hpp"
#include "data.hpp"
#include "view.hpp"
#include "model.hpp"
#include <Pangodream_18650_CL.h>


#define VOLTAGE_DIVIDER_PIN 34
Pangodream_18650_CL batteryLevel(VOLTAGE_DIVIDER_PIN, 1.72); //https://www.pangodream.es/esp32-getting-battery-charging-level

//#define TEST_WITH_DUMMY_DATA


#ifdef TEST_WITH_DUMMY_DATA
void dummYTestData () {
  data.batteries[0].isFilled=true;
  strncpy(data.batteries[0].key, "Cort. grande", KEY_LENGTH);
  data.batteries[0].value=90;
  data.batteries[1].isFilled=true;
  strncpy(data.batteries[1].key, "Climatizador", KEY_LENGTH);
  data.batteries[1].value=10;
  data.batteries[2].isFilled=true;
  strncpy(data.batteries[2].key, "Cort. dorm.", KEY_LENGTH);
  data.batteries[2].value=60;
  data.batteries[3].isFilled=false;
  strncpy(data.batteries[3].key, "Pantalla", KEY_LENGTH);
  data.batteries[3].value=60;
  strncpy(data.temperatures[0].key, "Dormitorio", KEY_LENGTH);
  data.temperatures[0].temp=22;
  data.temperatures[0].isTempFilled=true;
  data.temperatures[0].image=IMG_CAMA;
  strncpy(data.temperatures[1].key, "Casa", KEY_LENGTH);
  data.temperatures[1].temp=21;
  data.temperatures[1].isTempFilled=true;
  data.temperatures[1].image=IMG_CASA;
  data.covid.isFilled = true;
  data.covid.active = 430088;
  data.covid.newCases = 50000;
  data.covid.newKills = 500;
}
#endif

//forward function definitions
int32_t getWiFiChannel(const char *ssid);

void goSleep(){
    pinMode(13, OUTPUT); //this disables sd saves 4a
    digitalWrite(13, HIGH);
    gpio_deep_sleep_hold_en();
    esp_sleep_enable_ext0_wakeup((gpio_num_t)BUTTON_PIN, LOW);
    esp_sleep_enable_timer_wakeup(15L * 60L * 1000L * 1000L); // 15 min in nanosecs
    esp_deep_sleep_start();
}

void onReceive( response & rsp){
  mapResponse2Data(rsp);
  commPendingMessages -- ;
  if (commPendingMessages == 0) {//todos los mensajes pendientes recibidos
    renderData();
    goSleep();
  }
}


void setup()
{
    Serial.begin(115200);
    Serial.println();
    Serial.println("setup");
    data.batteries[DATA_INDEX_battSelf].value = batteryLevel.getBatteryChargeLevel();
    data.batteries[DATA_INDEX_battSelf].isFilled= true;
    strncpy(data.batteries[DATA_INDEX_battSelf].key, "Esta Pantalla", KEY_LENGTH);

    SPI.begin(SPI_CLK, SPI_MISO, SPI_MOSI, ELINK_SS);
    
    #ifdef TEST_WITH_DUMMY_DATA
    dummYTestData(); 
    #else 
    switch(esp_sleep_get_wakeup_cause()){
      case ESP_SLEEP_WAKEUP_EXT0 : break;  
      case ESP_SLEEP_WAKEUP_EXT1 : break; 
      case ESP_SLEEP_WAKEUP_TIMER : break; 
      case ESP_SLEEP_WAKEUP_TOUCHPAD : break;
      case ESP_SLEEP_WAKEUP_ULP : break;
      default:
        sharedChannel = getWiFiChannel(ssid); 
        break;
    }
    cnx.init(sharedChannel);
    cnx.onReceiveSomething = onReceive;
    commDoRequest();
    #endif
}


void loop()
{
  #ifndef TEST_WITH_DUMMY_DATA
  delay(5* 1000); 
  renderError("no response in 5s");
  #else
  renderData();
  #endif
  goSleep();
}


int32_t getWiFiChannel(const char *ssid) {
  if (int32_t n = WiFi.scanNetworks()) {
      for (uint8_t i=0; i<n; i++) {
          if (!strcmp(ssid, WiFi.SSID(i).c_str())) {
              return WiFi.channel(i);
          }
      }
  }
  return 0;
}

